import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')


#prvi
#cars_mpg_sort = mtcars.sort_values(by=['mpg'])
#print(mtcars_mpg_sort.head(5))
#
#drugi
#print(mtcars_mpg_sort[mtcars_mpg_sort.cyl==8].tail(3))
#
#treci
#cyls = mtcars[mtcars.cyl==6]
#print(cyls.mpg.mean())

#cetvrti
#cyls = mtcars[(mtcars.cyl==4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)]
#print(cyls.mpg.mean())

#peti
#print('Manual: ', mtcars.query('am==1').count().car)
#print('Automatic: ', mtcars[mtcars.am==0].count().car)

#sesti
#print(mtcars[(mtcars.am==0) & (mtcars.hp>100)].count().car)

#sedmi
#print(mtcars['wt'] * 1000 /2.205 )

