import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#prvi
#mc = mtcars.groupby('cyl').mean()
#mc.mpg.plot.bar()

#drugi
#mc = mtcars[['cyl', 'wt']]
#mc.boxplot(by='cyl')

#treci
#man = mtcars.groupby('am').mean()
#man.mpg.plot.bar()

#cetvrti
#plt.scatter(mtcars[mtcars.am==1].qsec, mtcars[mtcars.am==1].hp, color='red', label ='manual', s=mtcars['wt']*50)
#plt.scatter(mtcars[mtcars.am==0].qsec, mtcars[mtcars.am==0].hp, color='blue', label ='automatic', s=mtcars['wt']*50)
#plt.legend()

