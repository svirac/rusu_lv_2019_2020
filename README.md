# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Damian Svirac

## Izmjene:
1. Corrected "counting_words.py"
2. Added "hello_world.py"
