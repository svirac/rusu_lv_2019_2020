import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T


#train data
np.random.seed(242)
data = non_func(200)
x_train = data[:, 0]
y_train = data[:, 1]
x_train = x_train.reshape(-1, 1)

#test data
np.random.seed(12)
data2 = non_func(200)
x_test = data2[:, 0]
x_test = x_test.reshape(-1, 1)
y_test = data2[:, 1]

model = MLPRegressor(hidden_layer_sizes=(32,16), 
                        activation='relu',
                        alpha=0.0001,
                        batch_size=8,
                        max_iter=500)

model.fit(x_train, y_train)
y_predict = model.predict(x_test)

score = model.score(x_test, y_test)
print(score)