import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

def generate_data(n):
	
	#prva klasa
	n1 = int(n/2)
	x1_1 = np.random.normal(0.0, 2, (n1,1))
	
	#x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
	x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1))
	y_1 = np.zeros([n1,1])
	temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
	
	#druga klasa
	n2 = int(n - n/2)
	x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2)
	y_2 = np.ones([n2,1])
	temp2 = np.concatenate((x_2,y_2),axis = 1)
	data = np.concatenate((temp1,temp2),axis = 0)
	
	#permutiraj podatke
	indices = np.random.permutation(n)
	data = data[indices,:]
	
	return data

#train data
np.random.seed(242)
data = generate_data(200)
x_train = data[:,[0,1]]
x_train = preprocessing.scale(x_train)
y_train = data[:,2]

#test data
np.random.seed(12)
data2 = generate_data(100)
x_test = data2[:,[0,1]]
x_test = preprocessing.scale(x_test)
y_test = data2[:,2]


#model
model = MLPClassifier(hidden_layer_sizes=(24, 24), 
						activation='logistic', 
						alpha=0.0001, 
						batch_size=8, 
						max_iter=500)
model.fit(x_train, y_train)
y_predict = model.predict(x_test)


#prikaz crno bijele slike
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(x_train[:,0])-0.5:max(x_train[:,0])+0.5:.05,
                          min(x_train[:,1])-0.5:max(x_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = model.predict_proba(grid)[:, 1].reshape(x_grid.shape)
cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.scatter(x_train[:,0], x_train[:,1], c=y_train)


#konfuzna matrica
cfm = confusion_matrix(y_test, y_predict)
plot_confusion_matrix(cfm)
plt.show()

tp = cfm[1,1]
tn = cfm[0,0]
fp = cfm[0,1]
fn = cfm[1,0]

accuracy = (tp+tn)/(tp+tn+fp+fn)
precision = tp/(tp+fp)
recall = tp/(tp+fn)
specificity = tn/(tn+fp)

print("accuracy: ", accuracy, ", precision: ", 
		precision, "\nrecall: ", recall, 
		" specificity: ", specificity)
