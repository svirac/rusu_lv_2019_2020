import re

def printMail(mails):
    if mails:
        for mail in mails:
            name = mail.split('@')[0]
            name = name[1:]
            print(name)


with open('mbox-short.txt') as f:
    for line in f:  
        # sadrži najmanje jedno slovo 'a'
#        mails = re.findall('[\s<][.a-zA-Z0-9]*a[.a-zA-Z0-9]*@\S+', line)
#        printMail(mails)
        
        # sadrži točno jedno slovo 'a'
#        mails = re.findall('[\s<][.b-zB-Z0-9]*[aA][.b-zB-Z0-9]*@\S+', line)
#        printMail(mails)
        
        # ne sadrži slovo 'a'
#        mails = re.findall('[\s<][.b-zB-Z0-9]*@\S+', line)
#        printMail(mails)
        
        # sadrži jedan ili više numeričkih znakova
#        mails = re.findall('[\s<][.a-zA-Z0-9]*[0-9][.a-zA-Z0-9]*@\S+', line)
#        printMail(mails)
        
        # sadrži samo mala slova
#        mails = re.findall('[\s<][.a-z]+@\S+', line)
#        printMail(mails)