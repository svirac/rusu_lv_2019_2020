import re

fhand = open('mbox-short.txt')

for line in fhand:
    line = line.rstrip()
    mails = re.findall('[a-zA-Z0-9]+@\S+', line)
    if mails:
        for mail in mails:
            name = mail.split('@')[0]
            print (name)