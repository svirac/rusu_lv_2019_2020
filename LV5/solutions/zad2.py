from sklearn import datasets
import numpy as np
import scipy as sp
from sklearn import cluster
import matplotlib.pyplot as plt
import math


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

def calculateDistance(x1,y1,x2,y2):
    dist = math.sqrt((x2-x1)**2 + (y2-y1)**2)
    return dist

x = generate_data(500, 1)
cluster_list = []
distance_list = []

for i in range(2, 20):
    kmeans = cluster.KMeans(n_clusters=i)
    cluster_list.append(i)
    distance_list.append(kmeans.fit(x).inertia_)
    

plt.figure()
plt.plot(cluster_list, distance_list)
plt.show()



