fname = 'mbox-short.txt'
fhand = open(fname)

dict_host = dict()
emails = []
hostnames = []

# Find all email addresses and put them in list
for line in fhand:
    line = line.rstrip()
    if line.startswith('From:'):
        email = line[6:]
        if email not in emails:
            emails.append(email)
                
print("\nE-mails: ",emails,"\n")

# Make hostnames from emails
for email in emails:
    hostname = email.split("@",1)[1]
    if hostname not in hostnames:
        hostnames.append(hostname)
        
# Add to hostnames and their counts to dictionary
fhand.seek(0)
for line in fhand:
    for hostname in hostnames:
        if hostname in line: 
            if hostname in dict_host:
                dict_host[hostname] = dict_host[hostname] + 1
            else:
                dict_host[hostname] = 1

print("Dictionary:",dict_host)