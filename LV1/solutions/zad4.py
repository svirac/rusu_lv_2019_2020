x = input('Enter number: ')
try:
    num1 = float(x)
    cnt = 1
    sum_val = num1
    min_val = num1
    max_val = num1
except:
    print('Please enter a number!')  
 
    
while True:
    y = input('Enter number: ')
    try:
        num = float(y)
        cnt += 1
        sum_val += num
        if num < min_val:
            min_val = num
        if num > max_val:
            max_val = num
    except:
        print('Please enter a number!')
    if y in 'Done':
        break
           
    
avg = sum_val / cnt
print('Count:', cnt, ', Average:', avg, ', Min:', min_val, ', Max:', max_val)