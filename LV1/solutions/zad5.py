import sys

fname = input('Enter file name: ')
try:
    fhand = open(fname)
except:
    print('File cannot be opened', fname)
    sys.exit()

val_sum = 0
cnt = 0

for line in fhand:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence'):
        value = float(line[20:])
        cnt += 1
        val_sum += value
        
print(val_sum/cnt)