import numpy as np
from sklearn.datasets import fetch_openml
from sklearn.externals import joblib
from sklearn.neural_network import MLPClassifier
import pickle
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


#mnist = fetch_mldata('MNIST original')
X, y = fetch_openml(name='mnist_784', version=1, return_X_y=True)
#X, y = mnist.data, mnist.target

#print('Got MNIST with %d training- and %d test samples' % (len(X), len(y_test)))
#print('Image size is:')

# rescale the data, train/test split
X = X / 255.
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]


# TODO: build youw own neural network using sckitlearn MPLClassifier 
mlp_mnist = MLPClassifier(hidden_layer_sizes=(70,10), max_iter=20, 
                    verbose=10)

# TODO: evaluate trained NN
mlp_mnist.fit(X_train, y_train)
y_predict = mlp_mnist.predict(X_test)

# save NN to disk
filename = "NN_model.sav"
joblib.dump(mlp_mnist, filename)

print("Training set score: %f" % mlp_mnist.score(X_train, y_train))
print("Test set score: %f" % mlp_mnist.score(X_test, y_test))

#konfuzna matrica
# cfm = confusion_matrix(y_test, y_predict)

# tp = cfm[1,1]
# tn = cfm[0,0]
# fp = cfm[0,1]
# fn = cfm[1,0]

# accuracy = (tp+tn)/(tp+tn+fp+fn)
# precision = tp/(tp+fp)
# recall = tp/(tp+fn)
# specificity = tn/(tn+fp)

# print("accuracy: ", accuracy, ", precision: ", 
# 		precision, "\nrecall: ", recall, 
# 		" specificity: ", specificity)